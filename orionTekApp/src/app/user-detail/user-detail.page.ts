import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import * as moment from 'moment';
import { User } from '../models/user';
import { ApiService } from '../services/api.service';




@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.page.html',
  styleUrls: ['./user-detail.page.scss'],
})
export class UserDetailPage implements OnInit {

  id: number;
  data: any;
  addresses: any;


  constructor(
    private navCtrl: NavController,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public apiService: ApiService

  ) { }

  ionViewWillEnter() {
    this.id = this.activatedRoute.snapshot.params["id"];
    //get item details using id
    this.apiService.getItem(this.id).subscribe(response => {
      this.data = response.response;
      this.addresses = this.data.address;
    })
  }

  onClick(){
    this.router.navigate(['/home']);
  }

  ngOnInit() {
    this.data = '';

  }


  formatDate(date){
    return moment(date).format("MMMM Do,YYYY");             

  }

}
